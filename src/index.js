import * as Sketch from 'webgl-sketch'
// import fragmentShaderSource from '/shaders/eyeball.glsl';
import fragmentShaderSource from '/shaders/terrain.glsl'
import vertexShaderSource from '/shaders/basic-vertex.glsl'

const roff = () => Math.random() * 20 - 10

const seed = [
  (Math.random() < 0.5 ? -1 : 1) * Math.random() * 999.999,
  (Math.random() < 0.5 ? -1 : 1) * Math.random() * 999.999
]

window.console.log('Seed:', seed)

Sketch.create({
  canvas: document.querySelector('canvas'),
  fragmentShaderSource,
  size: [4096, 2048],
  // size: [8192, 4096],
  uniforms: {
    qxOffset: [roff(), roff()],
    qyOffset: [roff(), roff()],
    rxOffset: [roff(), roff()],
    ryOffset: [roff(), roff()],
    seed
  },
  vertexShaderSource
})
