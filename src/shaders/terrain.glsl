#version 100

precision mediump float;

#define HASHSCALE4 vec4(0.1031, 0.103, 0.0973, 0.1099)
#define M_2PI 6.2831853071795864769252867665590
#define M_PI 3.1415926535897932384626433832795

#define BLACK vec3(0.067, 0.067, 0.067)
#define BLUE vec3(0.000, 0.455, 0.851)
#define GREEN vec3(0.180, 0.800, 0.251)
#define NAVY vec3(0.000, 0.122, 0.247)
#define OLIVE vec3(0.239, 0.600, 0.439)
#define ORANGE vec3(1.000, 0.522, 0.114)
#define RED vec3(1.000, 0.255, 0.212)
#define SILVER vec3(0.867, 0.867, 0.867)
#define WHITE vec3(1.000, 1.000, 1.000)

#define ARID_1 vec3(0.996, 0.953, 0.851)
#define ARID_2 vec3(0.910, 0.835, 0.749)
#define HUMID_1 vec3(0.624, 0.737, 0.655)
#define HUMID_2 vec3(0.937, 0.965, 0.875)

uniform vec2 u_qxOffset;
uniform vec2 u_qyOffset;
uniform vec2 u_resolution;
uniform vec2 u_rxOffset;
uniform vec2 u_ryOffset;
uniform vec2 u_seed;

vec2 hash2(in vec2 p) {
  p = vec2(
    dot(p, vec2(127.1, 311.7)),
    dot(p, vec2(269.5, 183.3))
  );
  return fract(sin(p) + 43758.5453123);
}

vec3 hash3(in vec3 p) {
  p = vec3(
    dot(p, vec3(127.1, 311.7, 74.7)),
    dot(p, vec3(269.5, 183.3, 246.1)),
    dot(p, vec3(113.5, 271.9, 124.6))
  );
  return fract(sin(p) + 43758.5453123);
}

vec3 hash3(in vec2 p) {
  vec3 q = vec3(
    dot(p, vec2(127.1, 311.7)),
    dot(p, vec2(269.5, 183.3)),
    dot(p, vec2(419.2, 371.9))
  );
  return fract(sin(q) * 43758.5453123);
}

vec4 hash4(in vec3 p) {
  vec4 p4 = fract(vec4(p.xyzx) * HASHSCALE4);
  p4 += dot(p4, p4.wzxy + 19.19);
  return fract((p4.xxyz + p4.yzzw) * p4.zywx);
}

float noise(in vec2 p) {
  vec2 e = floor(p);
  vec2 f = fract(p);
  float t = 1.0 + 63.0 * pow(0.0, 4.0);
  float va = 0.0;
  float wt = 0.0;
  for (int j = -2; j <= 2; j++) {
    for (int i = -2; i <= 2; i++) {
      vec2 g = vec2(float(i), float(j));
      vec3 o = hash3(e + g) * vec3(0.0, 0.0, 1.0);
      vec2 r = g - f + o.xy;
      float d = dot(r, r);
      float ww = pow(1.0 - smoothstep(0.0, 1.414, sqrt(d)), t);
      va += o.z * ww;
      wt += ww;
    }
  }
  return va / wt;
}

float noise(in vec3 p) {
  vec3 e = floor(p);
  vec3 f = fract(p);
  float t = 1.0 + 63.0 * pow(0.0, 4.0);
  float va = 0.0;
  float wt = 0.0;
  for (int k = -2; k <= 2; k++) {
    for (int j = -2; j <= 2; j++) {
      for (int i = -2; i <= 2; i++) {
        vec3 g = vec3(float(i), float(j), float(k));
        vec4 o = hash4(e + g) * vec4(0.0, 0.0, 0.0, 1.0);
        vec3 r = g - f + o.xyz;
        float d = dot(r, r);
        float ww = pow(1.0 - smoothstep(0.0, 1.414, sqrt(d)), t);
        va += o.w * ww;
        wt += ww;
      }
    }
  }
  return va / wt;
}

float fbm(in int octaves, in vec2 p) {
  float value = 0.0;
  for (int o = 0; o < 100; o++) {
    if (o >= octaves) break;
    float m = pow(0.5, float(o + 1));
    value += m * noise(pow(2.0, float(o)) * p);
  }
  return value / (1.0 - pow(0.5, float(octaves)));
}

float cyl(in int octaves, in float scale, in vec2 p) {
  float circ = 4.0 * scale;
  float r = circ / M_2PI;
  float nx = scale * p.x / circ;
  float rads = nx * M_2PI;
  float a = r * sin(rads);
  float b = r * cos(rads);
  float value = 0.0;
  for (int o = 0; o < 100; o++) {
    if (o >= octaves) break;
    float m = pow(0.5, float(o + 1));
    value += m * noise(pow(2.0, float(o)) * vec3(a, scale * p.y, b));
  }
  return value / (1.0 - pow(0.5, float(octaves)));
}

float func2(in int octaves, in float scale, in vec2 p, out vec2 q) {
  q = vec2(
    cyl(octaves, scale, p + u_qxOffset),
    cyl(octaves, scale, p + u_qyOffset)
  );

  return cyl(octaves, scale, p + 4.0 * q);
}

float func3(in int octaves, in float scale, in vec2 p, out vec2 q, out vec2 r) {
  q = vec2(
    cyl(octaves, scale, p + u_qxOffset),
    cyl(octaves, scale, p + u_qyOffset)
  );

  r = vec2(
    cyl(octaves, scale, p + 4.0 * q + u_rxOffset),
    cyl(octaves, scale, p + 4.0 * q + u_ryOffset)
  );

  return cyl(octaves, scale, p + 4.0 * r);
}

void main() {
  vec2 uv = (2.0 * gl_FragCoord.xy - u_resolution.xy) / u_resolution.y;
  vec2 p = u_seed.xy + uv;

  vec2 q, r;
  float elevation = func3(6, 2.3, p, q, r);
  if (elevation > 0.55) {
    /*
    vec3 color = mix(
      vec3(0.996, 0.953, 0.851),
      vec3(0.449, 0.418, 0.386),
      1.0 - ((elevation - 0.55) / 0.45)
    );
    color = mix(color, vec3(0.40, 0.32, 0.22), dot(r, r));
    color = mix(color, SILVER, 0.5 * q.y * q.y);
    color = mix(
      color, vec3(0.7, 0.6, 0.5),
      0.5 * smoothstep(0.6, 1.4, abs(r.y) + abs(r.x))
    );
    color *= elevation * 2.0;

    float lat = 1.0 - abs(uv.y) / 0.5;

    // float humidity = 0.2 * lat + 0.8 * cyl(4, 4.3, p);
    // color = mix(color, vec3(0.439, 0.616, 0.529), sqrt(humidity));

    // float temperature = 0.4 * lat + 0.6 * func2(4, 3.7, p, q);
    // color = mix(color, vec3(0.980, 0.980, 1.0), 1.0 - temperature);

    gl_FragColor = vec4(color, 1.0);
    */
    gl_FragColor = vec4(0.0, 0.5, 0.1, 1.0);
  } else {
    gl_FragColor = vec4((0.35 + elevation) * vec3(0.0, 0.132, 0.247), 1.0);
  }
}
